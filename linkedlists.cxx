#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice){
            case 1:
                printList(head);
                break;
            
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            
            case 3:
                printf("Enter data to prepend:");
                scanf("%d",&data);
                prepend(&head,data);
                break;
                
            case 4:
                printf("1. Delete by key\n2.Delete by value\n");
                int choose;
                printf("Choice: ");
                scanf("%d",&choose);
                switch (choose){
                    case 1:
                        printf("Enter key of value to delete:");
                        int k;
                        scanf("%d",&k);
                        deleteByKey(&head,k);
                        break;
                        
                    case 2:
                        printf("Enter value to delete:");
                        scanf("%d",&data);
                        deleteByValue(&head,data);
                        break;
                }
        
            case 5:
                exit(1);
                break;  
                
            default:
                printf("Invalid choice. Please try again.\n"); 
    }
    }

    return 0;
}


// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.

struct Node *createNode(int num){
    struct Node*newNode=(struct Node*)malloc(sizeof(struct Node));
    if (newNode==NULL){
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
        }
    newNode->number=num;
    newNode->next=NULL;
    return newNode;
}
void printList(struct Node *head){
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key){
    struct Node*current=*head;
    struct Node*current1=*head;
    if (key==0){
       *head=current->next;
       return;
       }
    while (key!=0){
            current1=current;
            current=current1->next;
            key--;
        }
    current1->next=current->next;
    free(current);
    current=NULL;
}

void deleteByValue(struct Node **head, int value){
    struct Node*test=createNode(value);
    struct Node*current=*head;
    struct Node*ptr=*head;
    int counter=0;
    while (current!=NULL){
        counter++;
        if (test->number==current->number){
            if (counter==1){
                *head=current->next;
                return;
            }
            else{
                ptr->next=current->next;
                free(current);
                current=NULL;
                return;
            }
        }
        ptr=current;
        current=current->next;
    }
    
}
    
void insertAfterKey(struct Node **head, int key, int value){
    struct Node*test=createNode(value);
    struct Node*current=*head;
     if (*head==NULL){
        *head=test;
        return;
        }
    if (key==0){
        test->next=current->next;
        current->next=test;
        return;
        }
    while (key!=0){
        current=current->next;
        key--;
    }
    test->next=current->next;
    current->next=test;
    }

void insertAfterValue(struct Node **head, int searchValue, int newValue){
    struct Node*newNode=createNode(newValue);
    struct Node*oldNode=createNode(searchValue);
    struct Node*current=*head;
    if (*head==NULL){
        *head=newNode;
        return;
        }
    while (current != NULL){
         if (current->number==oldNode->number){
             newNode->next=current->next;
             current->next=newNode;
             return;
             }
         current=current->next;
         }        
    }